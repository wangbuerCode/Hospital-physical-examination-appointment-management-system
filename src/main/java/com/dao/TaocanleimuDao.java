package com.dao;

import com.entity.TaocanleimuEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.TaocanleimuVO;
import com.entity.view.TaocanleimuView;


/**
 * 套餐类目
 * 
 * @author 
 * @email 
 * @date 2021-04-04 03:43:51
 */
public interface TaocanleimuDao extends BaseMapper<TaocanleimuEntity> {
	
	List<TaocanleimuVO> selectListVO(@Param("ew") Wrapper<TaocanleimuEntity> wrapper);
	
	TaocanleimuVO selectVO(@Param("ew") Wrapper<TaocanleimuEntity> wrapper);
	
	List<TaocanleimuView> selectListView(@Param("ew") Wrapper<TaocanleimuEntity> wrapper);

	List<TaocanleimuView> selectListView(Pagination page,@Param("ew") Wrapper<TaocanleimuEntity> wrapper);
	
	TaocanleimuView selectView(@Param("ew") Wrapper<TaocanleimuEntity> wrapper);
	
}
