package com.dao;

import com.entity.ShangchuantijianbaogaoEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.ShangchuantijianbaogaoVO;
import com.entity.view.ShangchuantijianbaogaoView;


/**
 * 上传体检报告
 * 
 * @author 
 * @email 
 * @date 2021-04-04 03:43:51
 */
public interface ShangchuantijianbaogaoDao extends BaseMapper<ShangchuantijianbaogaoEntity> {
	
	List<ShangchuantijianbaogaoVO> selectListVO(@Param("ew") Wrapper<ShangchuantijianbaogaoEntity> wrapper);
	
	ShangchuantijianbaogaoVO selectVO(@Param("ew") Wrapper<ShangchuantijianbaogaoEntity> wrapper);
	
	List<ShangchuantijianbaogaoView> selectListView(@Param("ew") Wrapper<ShangchuantijianbaogaoEntity> wrapper);

	List<ShangchuantijianbaogaoView> selectListView(Pagination page,@Param("ew") Wrapper<ShangchuantijianbaogaoEntity> wrapper);
	
	ShangchuantijianbaogaoView selectView(@Param("ew") Wrapper<ShangchuantijianbaogaoEntity> wrapper);
	
}
