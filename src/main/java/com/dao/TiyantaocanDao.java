package com.dao;

import com.entity.TiyantaocanEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.TiyantaocanVO;
import com.entity.view.TiyantaocanView;


/**
 * 体验套餐
 * 
 * @author 
 * @email 
 * @date 2021-04-04 03:43:51
 */
public interface TiyantaocanDao extends BaseMapper<TiyantaocanEntity> {
	
	List<TiyantaocanVO> selectListVO(@Param("ew") Wrapper<TiyantaocanEntity> wrapper);
	
	TiyantaocanVO selectVO(@Param("ew") Wrapper<TiyantaocanEntity> wrapper);
	
	List<TiyantaocanView> selectListView(@Param("ew") Wrapper<TiyantaocanEntity> wrapper);

	List<TiyantaocanView> selectListView(Pagination page,@Param("ew") Wrapper<TiyantaocanEntity> wrapper);
	
	TiyantaocanView selectView(@Param("ew") Wrapper<TiyantaocanEntity> wrapper);
	
}
