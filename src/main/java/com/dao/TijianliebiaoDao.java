package com.dao;

import com.entity.TijianliebiaoEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.TijianliebiaoVO;
import com.entity.view.TijianliebiaoView;


/**
 * 体检列表
 * 
 * @author 
 * @email 
 * @date 2021-04-04 03:43:51
 */
public interface TijianliebiaoDao extends BaseMapper<TijianliebiaoEntity> {
	
	List<TijianliebiaoVO> selectListVO(@Param("ew") Wrapper<TijianliebiaoEntity> wrapper);
	
	TijianliebiaoVO selectVO(@Param("ew") Wrapper<TijianliebiaoEntity> wrapper);
	
	List<TijianliebiaoView> selectListView(@Param("ew") Wrapper<TijianliebiaoEntity> wrapper);

	List<TijianliebiaoView> selectListView(Pagination page,@Param("ew") Wrapper<TijianliebiaoEntity> wrapper);
	
	TijianliebiaoView selectView(@Param("ew") Wrapper<TijianliebiaoEntity> wrapper);
	
}
