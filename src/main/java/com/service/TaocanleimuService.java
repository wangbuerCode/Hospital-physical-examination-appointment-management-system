package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.TaocanleimuEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.TaocanleimuVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.TaocanleimuView;


/**
 * 套餐类目
 *
 * @author 
 * @email 
 * @date 2021-04-04 03:43:51
 */
public interface TaocanleimuService extends IService<TaocanleimuEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<TaocanleimuVO> selectListVO(Wrapper<TaocanleimuEntity> wrapper);
   	
   	TaocanleimuVO selectVO(@Param("ew") Wrapper<TaocanleimuEntity> wrapper);
   	
   	List<TaocanleimuView> selectListView(Wrapper<TaocanleimuEntity> wrapper);
   	
   	TaocanleimuView selectView(@Param("ew") Wrapper<TaocanleimuEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<TaocanleimuEntity> wrapper);
   	
}

