package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.TijianliebiaoEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.TijianliebiaoVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.TijianliebiaoView;


/**
 * 体检列表
 *
 * @author 
 * @email 
 * @date 2021-04-04 03:43:51
 */
public interface TijianliebiaoService extends IService<TijianliebiaoEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<TijianliebiaoVO> selectListVO(Wrapper<TijianliebiaoEntity> wrapper);
   	
   	TijianliebiaoVO selectVO(@Param("ew") Wrapper<TijianliebiaoEntity> wrapper);
   	
   	List<TijianliebiaoView> selectListView(Wrapper<TijianliebiaoEntity> wrapper);
   	
   	TijianliebiaoView selectView(@Param("ew") Wrapper<TijianliebiaoEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<TijianliebiaoEntity> wrapper);
   	
}

