package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.TaocanleimuDao;
import com.entity.TaocanleimuEntity;
import com.service.TaocanleimuService;
import com.entity.vo.TaocanleimuVO;
import com.entity.view.TaocanleimuView;

@Service("taocanleimuService")
public class TaocanleimuServiceImpl extends ServiceImpl<TaocanleimuDao, TaocanleimuEntity> implements TaocanleimuService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<TaocanleimuEntity> page = this.selectPage(
                new Query<TaocanleimuEntity>(params).getPage(),
                new EntityWrapper<TaocanleimuEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<TaocanleimuEntity> wrapper) {
		  Page<TaocanleimuView> page =new Query<TaocanleimuView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<TaocanleimuVO> selectListVO(Wrapper<TaocanleimuEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public TaocanleimuVO selectVO(Wrapper<TaocanleimuEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<TaocanleimuView> selectListView(Wrapper<TaocanleimuEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public TaocanleimuView selectView(Wrapper<TaocanleimuEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
