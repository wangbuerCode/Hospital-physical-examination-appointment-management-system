package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.ShangchuantijianbaogaoDao;
import com.entity.ShangchuantijianbaogaoEntity;
import com.service.ShangchuantijianbaogaoService;
import com.entity.vo.ShangchuantijianbaogaoVO;
import com.entity.view.ShangchuantijianbaogaoView;

@Service("shangchuantijianbaogaoService")
public class ShangchuantijianbaogaoServiceImpl extends ServiceImpl<ShangchuantijianbaogaoDao, ShangchuantijianbaogaoEntity> implements ShangchuantijianbaogaoService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<ShangchuantijianbaogaoEntity> page = this.selectPage(
                new Query<ShangchuantijianbaogaoEntity>(params).getPage(),
                new EntityWrapper<ShangchuantijianbaogaoEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<ShangchuantijianbaogaoEntity> wrapper) {
		  Page<ShangchuantijianbaogaoView> page =new Query<ShangchuantijianbaogaoView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<ShangchuantijianbaogaoVO> selectListVO(Wrapper<ShangchuantijianbaogaoEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public ShangchuantijianbaogaoVO selectVO(Wrapper<ShangchuantijianbaogaoEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<ShangchuantijianbaogaoView> selectListView(Wrapper<ShangchuantijianbaogaoEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public ShangchuantijianbaogaoView selectView(Wrapper<ShangchuantijianbaogaoEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
