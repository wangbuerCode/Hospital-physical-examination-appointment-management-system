package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.TijianliebiaoDao;
import com.entity.TijianliebiaoEntity;
import com.service.TijianliebiaoService;
import com.entity.vo.TijianliebiaoVO;
import com.entity.view.TijianliebiaoView;

@Service("tijianliebiaoService")
public class TijianliebiaoServiceImpl extends ServiceImpl<TijianliebiaoDao, TijianliebiaoEntity> implements TijianliebiaoService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<TijianliebiaoEntity> page = this.selectPage(
                new Query<TijianliebiaoEntity>(params).getPage(),
                new EntityWrapper<TijianliebiaoEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<TijianliebiaoEntity> wrapper) {
		  Page<TijianliebiaoView> page =new Query<TijianliebiaoView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<TijianliebiaoVO> selectListVO(Wrapper<TijianliebiaoEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public TijianliebiaoVO selectVO(Wrapper<TijianliebiaoEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<TijianliebiaoView> selectListView(Wrapper<TijianliebiaoEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public TijianliebiaoView selectView(Wrapper<TijianliebiaoEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
