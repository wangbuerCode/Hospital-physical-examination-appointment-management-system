package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.TiyantaocanDao;
import com.entity.TiyantaocanEntity;
import com.service.TiyantaocanService;
import com.entity.vo.TiyantaocanVO;
import com.entity.view.TiyantaocanView;

@Service("tiyantaocanService")
public class TiyantaocanServiceImpl extends ServiceImpl<TiyantaocanDao, TiyantaocanEntity> implements TiyantaocanService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<TiyantaocanEntity> page = this.selectPage(
                new Query<TiyantaocanEntity>(params).getPage(),
                new EntityWrapper<TiyantaocanEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<TiyantaocanEntity> wrapper) {
		  Page<TiyantaocanView> page =new Query<TiyantaocanView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<TiyantaocanVO> selectListVO(Wrapper<TiyantaocanEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public TiyantaocanVO selectVO(Wrapper<TiyantaocanEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<TiyantaocanView> selectListView(Wrapper<TiyantaocanEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public TiyantaocanView selectView(Wrapper<TiyantaocanEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
