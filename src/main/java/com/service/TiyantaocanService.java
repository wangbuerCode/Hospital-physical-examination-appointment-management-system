package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.TiyantaocanEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.TiyantaocanVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.TiyantaocanView;


/**
 * 体验套餐
 *
 * @author 
 * @email 
 * @date 2021-04-04 03:43:51
 */
public interface TiyantaocanService extends IService<TiyantaocanEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<TiyantaocanVO> selectListVO(Wrapper<TiyantaocanEntity> wrapper);
   	
   	TiyantaocanVO selectVO(@Param("ew") Wrapper<TiyantaocanEntity> wrapper);
   	
   	List<TiyantaocanView> selectListView(Wrapper<TiyantaocanEntity> wrapper);
   	
   	TiyantaocanView selectView(@Param("ew") Wrapper<TiyantaocanEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<TiyantaocanEntity> wrapper);
   	
}

