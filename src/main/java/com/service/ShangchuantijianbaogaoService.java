package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.ShangchuantijianbaogaoEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.ShangchuantijianbaogaoVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.ShangchuantijianbaogaoView;


/**
 * 上传体检报告
 *
 * @author 
 * @email 
 * @date 2021-04-04 03:43:51
 */
public interface ShangchuantijianbaogaoService extends IService<ShangchuantijianbaogaoEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<ShangchuantijianbaogaoVO> selectListVO(Wrapper<ShangchuantijianbaogaoEntity> wrapper);
   	
   	ShangchuantijianbaogaoVO selectVO(@Param("ew") Wrapper<ShangchuantijianbaogaoEntity> wrapper);
   	
   	List<ShangchuantijianbaogaoView> selectListView(Wrapper<ShangchuantijianbaogaoEntity> wrapper);
   	
   	ShangchuantijianbaogaoView selectView(@Param("ew") Wrapper<ShangchuantijianbaogaoEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<ShangchuantijianbaogaoEntity> wrapper);
   	
}

