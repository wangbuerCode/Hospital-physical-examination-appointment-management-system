package com.entity.model;

import com.entity.TijianliebiaoEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
 

/**
 * 体检列表
 * 接收传参的实体类  
 *（实际开发中配合移动端接口开发手动去掉些没用的字段， 后端一般用entity就够用了） 
 * 取自ModelAndView 的model名称
 * @author 
 * @email 
 * @date 2021-04-04 03:43:51
 */
public class TijianliebiaoModel  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 科室
	 */
	
	private String keshi;
		
	/**
	 * 机房
	 */
	
	private String jifang;
		
	/**
	 * 检测项目
	 */
	
	private String jiancexiangmu;
		
	/**
	 * 预约开始时间
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	private Date yuyuekaishishijian;
		
	/**
	 * 预约结束时间
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	private Date yuyuejieshushijian;
		
	/**
	 * 体检医生
	 */
	
	private String tijianyisheng;
		
	/**
	 * 医生电话
	 */
	
	private String yishengdianhua;
		
	/**
	 * 预约金额
	 */
	
	private Integer yuyuejine;
		
	/**
	 * 设备图片
	 */
	
	private String shebeitupian;
				
	
	/**
	 * 设置：科室
	 */
	 
	public void setKeshi(String keshi) {
		this.keshi = keshi;
	}
	
	/**
	 * 获取：科室
	 */
	public String getKeshi() {
		return keshi;
	}
				
	
	/**
	 * 设置：机房
	 */
	 
	public void setJifang(String jifang) {
		this.jifang = jifang;
	}
	
	/**
	 * 获取：机房
	 */
	public String getJifang() {
		return jifang;
	}
				
	
	/**
	 * 设置：检测项目
	 */
	 
	public void setJiancexiangmu(String jiancexiangmu) {
		this.jiancexiangmu = jiancexiangmu;
	}
	
	/**
	 * 获取：检测项目
	 */
	public String getJiancexiangmu() {
		return jiancexiangmu;
	}
				
	
	/**
	 * 设置：预约开始时间
	 */
	 
	public void setYuyuekaishishijian(Date yuyuekaishishijian) {
		this.yuyuekaishishijian = yuyuekaishishijian;
	}
	
	/**
	 * 获取：预约开始时间
	 */
	public Date getYuyuekaishishijian() {
		return yuyuekaishishijian;
	}
				
	
	/**
	 * 设置：预约结束时间
	 */
	 
	public void setYuyuejieshushijian(Date yuyuejieshushijian) {
		this.yuyuejieshushijian = yuyuejieshushijian;
	}
	
	/**
	 * 获取：预约结束时间
	 */
	public Date getYuyuejieshushijian() {
		return yuyuejieshushijian;
	}
				
	
	/**
	 * 设置：体检医生
	 */
	 
	public void setTijianyisheng(String tijianyisheng) {
		this.tijianyisheng = tijianyisheng;
	}
	
	/**
	 * 获取：体检医生
	 */
	public String getTijianyisheng() {
		return tijianyisheng;
	}
				
	
	/**
	 * 设置：医生电话
	 */
	 
	public void setYishengdianhua(String yishengdianhua) {
		this.yishengdianhua = yishengdianhua;
	}
	
	/**
	 * 获取：医生电话
	 */
	public String getYishengdianhua() {
		return yishengdianhua;
	}
				
	
	/**
	 * 设置：预约金额
	 */
	 
	public void setYuyuejine(Integer yuyuejine) {
		this.yuyuejine = yuyuejine;
	}
	
	/**
	 * 获取：预约金额
	 */
	public Integer getYuyuejine() {
		return yuyuejine;
	}
				
	
	/**
	 * 设置：设备图片
	 */
	 
	public void setShebeitupian(String shebeitupian) {
		this.shebeitupian = shebeitupian;
	}
	
	/**
	 * 获取：设备图片
	 */
	public String getShebeitupian() {
		return shebeitupian;
	}
			
}
