package com.entity.model;

import com.entity.TiyantaocanEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
 

/**
 * 体验套餐
 * 接收传参的实体类  
 *（实际开发中配合移动端接口开发手动去掉些没用的字段， 后端一般用entity就够用了） 
 * 取自ModelAndView 的model名称
 * @author 
 * @email 
 * @date 2021-04-04 03:43:51
 */
public class TiyantaocanModel  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 套餐价格
	 */
	
	private Integer taocanjiage;
		
	/**
	 * 套餐类目
	 */
	
	private String taocanleimu;
		
	/**
	 * 套餐详情
	 */
	
	private String taocanxiangqing;
		
	/**
	 * 上架时间
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	private Date shangjiashijian;
		
	/**
	 * 套餐图片
	 */
	
	private String taocantupian;
				
	
	/**
	 * 设置：套餐价格
	 */
	 
	public void setTaocanjiage(Integer taocanjiage) {
		this.taocanjiage = taocanjiage;
	}
	
	/**
	 * 获取：套餐价格
	 */
	public Integer getTaocanjiage() {
		return taocanjiage;
	}
				
	
	/**
	 * 设置：套餐类目
	 */
	 
	public void setTaocanleimu(String taocanleimu) {
		this.taocanleimu = taocanleimu;
	}
	
	/**
	 * 获取：套餐类目
	 */
	public String getTaocanleimu() {
		return taocanleimu;
	}
				
	
	/**
	 * 设置：套餐详情
	 */
	 
	public void setTaocanxiangqing(String taocanxiangqing) {
		this.taocanxiangqing = taocanxiangqing;
	}
	
	/**
	 * 获取：套餐详情
	 */
	public String getTaocanxiangqing() {
		return taocanxiangqing;
	}
				
	
	/**
	 * 设置：上架时间
	 */
	 
	public void setShangjiashijian(Date shangjiashijian) {
		this.shangjiashijian = shangjiashijian;
	}
	
	/**
	 * 获取：上架时间
	 */
	public Date getShangjiashijian() {
		return shangjiashijian;
	}
				
	
	/**
	 * 设置：套餐图片
	 */
	 
	public void setTaocantupian(String taocantupian) {
		this.taocantupian = taocantupian;
	}
	
	/**
	 * 获取：套餐图片
	 */
	public String getTaocantupian() {
		return taocantupian;
	}
			
}
