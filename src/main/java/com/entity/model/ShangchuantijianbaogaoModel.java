package com.entity.model;

import com.entity.ShangchuantijianbaogaoEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
 

/**
 * 上传体检报告
 * 接收传参的实体类  
 *（实际开发中配合移动端接口开发手动去掉些没用的字段， 后端一般用entity就够用了） 
 * 取自ModelAndView 的model名称
 * @author 
 * @email 
 * @date 2021-04-04 03:43:51
 */
public class ShangchuantijianbaogaoModel  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 姓名
	 */
	
	private String xingming;
		
	/**
	 * 报告
	 */
	
	private String baogao;
		
	/**
	 * 上传时间
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	private Date shangchuanshijian;
		
	/**
	 * 上传报告
	 */
	
	private String shangchuanbaogao;
				
	
	/**
	 * 设置：姓名
	 */
	 
	public void setXingming(String xingming) {
		this.xingming = xingming;
	}
	
	/**
	 * 获取：姓名
	 */
	public String getXingming() {
		return xingming;
	}
				
	
	/**
	 * 设置：报告
	 */
	 
	public void setBaogao(String baogao) {
		this.baogao = baogao;
	}
	
	/**
	 * 获取：报告
	 */
	public String getBaogao() {
		return baogao;
	}
				
	
	/**
	 * 设置：上传时间
	 */
	 
	public void setShangchuanshijian(Date shangchuanshijian) {
		this.shangchuanshijian = shangchuanshijian;
	}
	
	/**
	 * 获取：上传时间
	 */
	public Date getShangchuanshijian() {
		return shangchuanshijian;
	}
				
	
	/**
	 * 设置：上传报告
	 */
	 
	public void setShangchuanbaogao(String shangchuanbaogao) {
		this.shangchuanbaogao = shangchuanbaogao;
	}
	
	/**
	 * 获取：上传报告
	 */
	public String getShangchuanbaogao() {
		return shangchuanbaogao;
	}
			
}
