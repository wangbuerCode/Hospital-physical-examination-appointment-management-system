package com.entity.view;

import com.entity.TijianliebiaoEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 体检列表
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author 
 * @email 
 * @date 2021-04-04 03:43:51
 */
@TableName("tijianliebiao")
public class TijianliebiaoView  extends TijianliebiaoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public TijianliebiaoView(){
	}
 
 	public TijianliebiaoView(TijianliebiaoEntity tijianliebiaoEntity){
 	try {
			BeanUtils.copyProperties(this, tijianliebiaoEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
